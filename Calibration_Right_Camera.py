####################################################
#Calibration of right camera
import numpy as np
import cv2.cv as cv
import cv2
import os
import common
import sys
import getopt
import glob
import itertools

#USAGE = '''USAGE: calib.py [--save <filename>] [--debug <outputpath>] [--square_size] [<image mask>]'''

args , img_mask = getopt.getopt(sys.argv[1:],'',  ['save=', 'debug=', 'square_size='])
args = dict(args)
try: img_mask = img_mask[0]
except: img_mask = '..//final//right//image*.png' #getting images from dir
img_names = glob.glob(img_mask)
debug_dir = args.get('--debug')
square_size = float(args.get('--square_size',3.5))
pattern_size = (7,7)
pattern_points = np.zeros( (np.prod(pattern_size), 3), np.float32)
pattern_points[:,:2] = np.indices(pattern_size).T.reshape(-1, 2)
pattern_points *= square_size
obj_points = [] #creates an object point matrix
img_points = [] #creates an image point matrix
h, w = 0, 0     #initializations
for fn in img_names:    #looking up in all the images
    print 'processing %s...' % fn,
    img = cv2.imread(fn, 0)
    h, w = img.shape[:2]
    found, corners = cv2.findChessboardCorners(img, pattern_size)
    if found:
        term = ( cv2.TERM_CRITERIA_EPS + cv2.TERM_CRITERIA_COUNT, 30, 0.1)
        cv2.cornerSubPix(img, corners, (4, 4), (-1, -1), term)
    if debug_dir:
        vis = cv2.cvtColor(img, cv2.COLOR_GRAY2BGR)
        cv2.drawChessboardCorners(vis, pattern_size, corners, found)
        path, name, ext = splitfn(fn)
        cv2.imwrite('%s/%s_chess.bmp' % (debug_dir, name), vis)
    if not found:
        print 'chessboard not found'
        continue
    img_points.append(corners.reshape(-1, 2))
    obj_points.append(pattern_points)

    print 'ok'

rms, camera_matrix, dist_coefs, rvecs, tvecs = cv2.calibrateCamera(obj_points, img_points, (w, h))
#f = open("C:\Users\Administrator\Desktop\calibration_camera.xml","w")
print "RMS:", rms
print "camera matrix:\n", camera_matrix
print "distortion coefficients: ", dist_coefs.ravel()



RMS = 0.334999208953
camera_matrix=[[ 796.55190184 ,   0.         , 305.37470194],
 [   0.         , 798.84010123 , 278.07322739],
 [   0.         ,   0.         ,   1.        ]]
dist_coefs =   [ -4.25098826e-01 ,  2.98349888e+00 ,  5.38156507e-03 ,-1.40448249e-03,
  -7.69026074e+00]

#Dumping data into a YAML file
data = {"camera_matrix":camera_matrix,"distortion_coefficients":dist_coefs}
import yaml
fname = "Right.yaml"
with open(fname,"w") as f:
    yaml.dump(data,f)


#####################################################################################################
