import cv2.cv as cv
import cv2
import numpy as np 
class Kalman(object):
	vx=1
	vy=1
	def __init__(self):
		self.kalman = cv.CreateKalman(4, 2, 0)
		self.kalman_state = cv.CreateMat(4, 1, cv.CV_32FC1)
		self.kalman_process_noise = cv.CreateMat(4, 1, cv.CV_32FC1)
		self.kalman_measurement = cv.CreateMat(2, 1, cv.CV_32FC1)
		# self.vx,self.vy = 1,1

	def setKalmanFilter(self,x,y):
		global vx , vy;
		# set previous state for prediction
		self.kalman.state_pre[0,0]  = x
		self.kalman.state_pre[1,0]  = y
		self.kalman.state_pre[2,0]  = self.vx # vx
		self.kalman.state_pre[3,0]  = self.vy # vy
		 
		# set self.kalman transition matrix
		self.kalman.transition_matrix[0,0] = 1
		self.kalman.transition_matrix[0,1] = 0
		self.kalman.transition_matrix[0,2] = 0
		self.kalman.transition_matrix[0,3] = 0
		self.kalman.transition_matrix[1,0] = 0
		self.kalman.transition_matrix[1,1] = 1
		self.kalman.transition_matrix[1,2] = 0
		self.kalman.transition_matrix[1,3] = 0
		self.kalman.transition_matrix[2,0] = 0
		self.kalman.transition_matrix[2,1] = 0
		self.kalman.transition_matrix[2,2] = 0
		self.kalman.transition_matrix[2,3] = 1
		self.kalman.transition_matrix[3,0] = 0
		self.kalman.transition_matrix[3,1] = 0
		self.kalman.transition_matrix[3,2] = 0
		self.kalman.transition_matrix[3,3] = 1
		 
		# set self.Kalman Filter
		cv.SetIdentity(self.kalman.measurement_matrix, cv.RealScalar(1))
		cv.SetIdentity(self.kalman.process_noise_cov, cv.RealScalar(1e-5))
		cv.SetIdentity(self.kalman.measurement_noise_cov, cv.RealScalar(1e-1))
		cv.SetIdentity(self.kalman.error_cov_post, cv.RealScalar(1))

	def changeKalmanMeasurements(self,x,y):
		self.kalman_measurement[0, 0] = x
		self.kalman_measurement[1, 0] = y


	def predictKalman(self):
		self.kalman_prediction = cv.KalmanPredict(self.kalman)
		predict_pt  = (self.kalman_prediction[0,0], self.kalman_prediction[1,0])
		# print predict_pt
		return predict_pt[0],predict_pt[1]
	
	def correction(self):
		self.kalman_estimated = cv.KalmanCorrect(self.kalman, self.kalman_measurement)
		state_p = (self.kalman_estimated[0,0], self.kalman_estimated[1,0])
		# print self.vx,self.vy