from serial import Serial
import threading
import math
from collections import deque
import numpy as np 

class Compute(threading.Thread):
	def __init__(self , threadID):
		threading.Thread.__init__(self);
		self.threadName = threadID;
		self.focal = 6.79243932e+02 ;
		self.disparity = 1;
		self.depth = 0;
		self.xL = 0;
		self.yL = 0;
		self.xR = 0;
		self.yR = 0;
		self.x3D = 0;
		self.y3D = 0;
		self.z3D = 0;
		self.preVel=None;
		self.fps= 22;
		self.distance_between_camera=77.0 # in cm
		# self.arduino = Serial('/dev/ttyUSB0', 9600)
		self.myQue = deque()
		self.servoX,self.servoY=0,0;

		

	def setParams(self, name , listin):
		self.list =listin
		# print self.threadName,'and value is :',self.list
		try:
			if name == "leftCamera":
				self.xL = self.list[0][0];
				self.yL = self.list[0][1];
			elif name == "rightCamera":
				self.xR = self.list[0][0];
				self.yR = self.list[0][1];
			else:
				print 'Undefined thread'
			exit(0);
		except:
			pass;
		finally:
			self.depthValue()
			self.get3DCoordinates();
			self.servo();


	def findvel(self,currentVel):
		try:
			vx = (currentVel[0]-self.preVel[0])/self.fps;
			vy = (currentVel[1] - self.preVel[1])/self.fps;
			self.preVel = currentVel;
			return vx,vy
		except:
			print "Error in velocity finding"
	


	def depthValue(self):
		print type(self.xL) , type(self.xR)
		self.disparity  = abs(self.xL - self.xR);
		try:
			self.depth = (6.79243932e+02)*self.distance_between_camera/self.disparity;
		except ZeroDivisionError as e :
			print 'Zero Division Error',e;
		print 'Depth of object  = ', self.depth;

	

	def get3DCoordinates(self):
		self.x3D = self.xL * self.depth /self.focal;
		self.y3D = self.yL * self.depth /self.focal;
		self.z3D = self.depth;
		print "WRT Left camera frame : 3D coordintate are : ",'(',self.x3D,',' ,self.y3D,',',self.z3D,')';

		



	def servo(self):
		if self.x3D is 0:
			self.x3D = 1;
		if self.x3D > self.servoX:
			deltaX = self.x3D-self.servoX
			deltaY = abs(self.y3D-self.servoY)
			currentAngle = math.pi - np.arctan(deltaY/deltaX);
			currentAngle = (currentAngle*180.0)/math.pi ;
			currentAngle=int(currentAngle);
		elif self.x3D < self.servoX:
			deltaX = self.servoX - self.x3D;
			deltaY = abs(self.y3D-self.servoY)
			currentAngle = math.pi - np.arctan(deltaY/deltaX);
			currentAngle = (currentAngle*180.0)/math.pi ;
			currentAngle=int(currentAngle);
		elif self.x3D is self.servoX:
			currentAngle=90;
		else:
			currentAngle=0;

		if (len(self.myQue)<=25):
			self.myQue.append(currentAngle);
			if (len(self.myQue)==25):
				print self.myQue;
				print"Current length is %f"%sum(self.myQue);
				x = sum(self.myQue)/len(self.myQue);

				servoAngle = self.myQue.popleft();
				servoAngle = str(servoAngle);
				servoAngle=servoAngle+'\n';
				print servoAngle;
				# self.arduino.write(servoAngle);
			else:
				pass;
		else:
			pass;
