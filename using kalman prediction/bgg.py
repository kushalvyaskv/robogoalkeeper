
import cv2
import numpy as np 
# import winsound
import threading 
import sys
import serial
import time
from collections import deque
import video
from bggcompute import Compute
from Kalman import Kalman
from arduinoCommunicator import arduinoCommunicator

# code for robo - goalkeeper arm ... :-) --->>>!!!!!

class App(threading.Thread):
	"""Initializing camera and then automatically fixing the object to be tracked"""
	def __init__(self, id , threadID) :
		threading.Thread.__init__(self);
		self.kalmanObject = Kalman()
		self.threadID = threadID;
		self.id = id;
		self.xc = 0;self.yc = 0;
		self.cam = cv2.VideoCapture(self.id);
		# self.lower = np.array([100,50,50],dtype=np.uint8);
		# self.upper = np.array([135,255,255],dtype=np.uint8);
		self.lower = np.array([0,0,0],dtype=np.uint8);
		self.upper = np.array([255,255,255],dtype=np.uint8);
		#reading the camera object:
		self.counter = 0;

		while(self.counter!=50):
			ret , self.frame = self.cam.read();
			print ret;
			if ret:
				# cv2.imshow('myframe',self.frame);
				self.counter+=1;
			if not ret:
				print "Failed to initialize camera";					
			print ret;
			cv2.imwrite('clicked.jpg' , self.frame);
		print 'Acquired Auxillary Image';
		# self.img = cv2.imread('clicked.jpg');
		# cv2.imshow('clke',self.img);
		# cv2.waitKey();
		self.selection=True;
		self.tracking_state = 0;
		self.show_backproj = False;
		cv2.destroyAllWindows();
		img = cv2.imread('clicked.jpg');
		# self.threshold(img);
		self.threshold_by_blue(img);
		# initializing objects for the compute class:
		self.computeObject = Compute(self.threadID);
		self.cam.release();
		cv2.destroyAllWindows();
	def threshold_by_blue(self,imhsv):
		imhsv = cv2.cvtColor(imhsv , cv2.COLOR_BGR2HSV);
		# lower_blue = np.array([90,50,50],dtype=np.uint8);
		# upper_blue = np.array([135,255,255],dtype=np.uint8);
		lower_blue = np.array([0,0,0],dtype=np.uint8);
		upper_blue = np.array([255,255,255],dtype=np.uint8);
		mask = cv2.inRange(imhsv, lower_blue, upper_blue);
		mask = cv2.morphologyEx(mask,cv2.MORPH_OPEN,np.ones((10,10),np.uint8));
		contours,heir = cv2.findContours(mask,cv2.RETR_LIST,cv2.CHAIN_APPROX_SIMPLE) 
		sx,sy,c=0,0,0
		for cnt in contours:
			c+=1;
			cnt = cv2.approxPolyDP(cnt,0.1*cv2.arcLength(cnt,True),True) # approximates the contours into 3 and 4
			if len(cnt) :
				moments = cv2.moments(cnt);
				# marks the center coordinates 
				if moments['m00']!=0:
					# cx and cy are centroids of the respective shapes
					cx = int(moments['m10']/moments['m00'])         # cx = M10/M00
					cy = int(moments['m01']/moments['m00'])         # cy = M01/M00
					sx += cx
					sy += cy
		if c is not 0:
			self.xc = sx/c
			self.yc = sy/c
		else:
			print "\n00000000000000000000000000000000000\n"

		print self.xc , self.yc
		


		 # commented as not efficient technique for thresholding 

	# def threshold(self , img):
	# 	print '\nentered threshold function\n';
	# 	# thresholding function:
	# 	newimg = img;
	# 	# convert image into gray scale and then threshold
	# 	img =  cv2.cvtColor(img , cv2.COLOR_BGR2GRAY);
	# 	__,thresh = cv2.threshold(img,180,255,cv2.THRESH_BINARY);
	# 	print 'hiabcd';
	# 	print img;
	# 	# cv2.imshow('threshold',thresh);
	# 	# cv2.imshow('original',img);
	# 	# if ret:
	# 	count=0;
	# 	self.xc,self.yc = 0,0;
	# 	x=[];
	# 	y=[];
	# 	for i in range(480):
	# 		for j in range(640):
	# 			if(thresh[i][j]==255):
	# 				x.append(j);
	# 				y.append(i);
	# 				# print count;
	# 				count+=1;
	# 				# print count;
	# 	if count is not 0:
	# 		self.xc = sum(x)/count;
	# 		self.yc = sum(y)/count;
	# 	print "Binary Image center is located at :",self.xc,self.yc;
	# 	cv2.circle(newimg , (self.xc,self.yc),50,3,);
	# 	# print self.xc;
	# 	# print ret;
	# 	# cv2.imshow('frame',newimg);
	# 	# cv2.waitKey();
	# 	cv2.destroyAllWindows();


	def ball_present(self):
		self.mask = cv2.morphologyEx(self.mask,cv2.MORPH_OPEN,np.ones((10,10),np.uint8));
		contours,heir = cv2.findContours(self.mask,cv2.RETR_LIST,cv2.CHAIN_APPROX_SIMPLE) 
		sx,sy,c=0,0,0
		for cnt in contours:
			c+=1;
			cnt = cv2.approxPolyDP(cnt,0.1*cv2.arcLength(cnt,True),True) # approximates the contours into 3 and 4
			if len(cnt) :
				moments = cv2.moments(cnt);
				# marks the center coordinates 
				if moments['m00']!=0:
					# cx and cy are centroids of the respective shapes
					cx = int(moments['m10']/moments['m00'])         # cx = M10/M00
					cy = int(moments['m01']/moments['m00'])         # cy = M01/M00
					sx += cx
					sy += cy
		if c is not 0:
			return True;
		else :
			return False;

	def run(self):
		global sysExit ; 
		global img;
		while True:
			self.cam = cv2.VideoCapture(self.id);

			while True:

				retr , self.frame = self.cam.read();

				vis = self.frame.copy();
				hsv = cv2.cvtColor(self.frame, cv2.COLOR_BGR2HSV);
				# self.mask = cv2.inRange(hsv, np.array((0., 60., 32.)), np.array((180., 255., 255.)));
				self.mask = cv2.inRange(hsv,self.lower,self.upper );

				if retr:	
					if self.selection:
						x0, y0, x1, y1 = self.xc , self.yc , self.xc+20 , self.yc+20;
						self.track_window = (x0,y0,x1,y1);
						hsv_roi = hsv[y0:y1, x0:x1];
						self.mask_roi = self.mask[y0:y1, x0:x1];
						hist = cv2.calcHist( [hsv_roi], [0], self.mask_roi, [16], [0, 180] );
						cv2.normalize(hist, hist, 0, 255, cv2.NORM_MINMAX);
						self.hist = hist.reshape(-1);
						vis_roi = vis[y0:y1, x0:x1];
						cv2.bitwise_not(vis_roi, vis_roi);
						vis[self.mask == 0] = 0;
						self.tracking_state=1;
						self.selection=False;

					# x = self.ball_present()
					if True:
					#  add some condition which will confirm that ball is in picture

						if self.tracking_state == 1:
							# self.selection = False ;
							prob = cv2.calcBackProject([hsv], [0], self.hist, [0, 180], 1);
							prob &= self.mask;
							term_crit = ( cv2.TERM_CRITERIA_EPS | cv2.TERM_CRITERIA_COUNT, 10, 1 );
							# print np.sum(prob)

							# if np:
							try:
								print "Entererd the try block"
								self.track_box, self.track_window = cv2.CamShift(prob, self.track_window, term_crit);
								self.kalmanObject.setKalmanFilter(self.track_box[0][0],self.track_box[0][1])
								self.kalmanObject.changeKalmanMeasurements(self.track_box[0][0],self.track_box[0][1])
								ax = self.track_box[0]
								ay = self.track_box[1]
								# print type(self.track_box[0])
							
							except Exception as e :
								print e,'\n';
								ax,ay = self.kalmanObject.predictKalman()
								self.kalmanObject.correction()
								print 'exception in try block\n'

							else :
								# gets executed if no exception occurs i.e. try block successfully executed
								print '\success \n';
							finally:	
								self.computeObject.setParams(self.threadID , [ax,ay])
								try:
									self.kalmanObject.vx,self.kalmanObject.vy=self.computeObject.findvel([ax,ay]);
								except:
									pass;
								
							
							try:
								# print ax,ay
								cv2.circle(vis,(ax,ay),50,3);
							except:
								# pass
								print "CIRCLE NOT ABLE TO BE DRAWN"

						cv2.imshow(self.threadID, vis);
					
				if cv2.waitKey(1) & 0xFF == ord('q'):
					cv2.destroyAllWindows();  
					sysExit = True;
					break;
			if sysExit:
				break;




if __name__ == '__main__':
	def loadCameraData():
		pass;
	def fixParams():
		pass;

	"""
	objLeft = App(0,'left');
	objLeft.threshold(img);
	print objLeft.xc , objLeft.yc;
	cv2.destroyAllWindows();	
	# objRight = App(0);
	# img = cv2.imread('clicked.jpg');
	# objRight.threshold(img);
	leftThread = Thread(target=objLeft.run);
	print objLeft.threadID;
	# rightThread = Thread(target = objRight.run);
	leftThread.run();
	# objLeft.run();
	print 'Finished';
	exit(0);
	"""

	
	sysExit = False;
	thread1 = App( 0, 'leftCamera');
	# thread2 = App( 2 , 'rightCamera');
	thread1.start();
	# thread2.start()
	while True:
		if sysExit:
			thread1.join();
			# thread2.join();
			break;
	print 'Finished';
	exit(0);