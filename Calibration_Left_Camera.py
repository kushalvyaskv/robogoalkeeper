#########################################
#Calibration of left camera
import numpy as np
import cv2
import os
import common
import sys
import getopt
import glob
import cv2.cv as cv
import itertools

#USAGE = '''USAGE: calib.py [--save <filename>] [--debug <outputpath>] [--square_size] [<image mask>]'''

args , img_mask = getopt.getopt(sys.argv[1:],'',  ['save=', 'debug=', 'square_size='])
args = dict(args)
try: img_mask = img_mask[0]
except: img_mask = '..//final//left//image*.png' #getting images from dir
img_names = glob.glob(img_mask)
debug_dir = args.get('--debug')
square_size = float(args.get('--square_size',3.5))
pattern_size = (7,7)
pattern_points = np.zeros( (np.prod(pattern_size), 3), np.float32)
pattern_points[:,:2] = np.indices(pattern_size).T.reshape(-1, 2)
pattern_points *= square_size
obj_points = [] #creates an object point matrix
img_points = [] #creates an image point matrix
h, w = 0, 0     #initializations
for fn in img_names:    #looking up in all the images
    print 'processing %s...' % fn,
    img = cv2.imread(fn, 0)
    h, w = img.shape[:2]
    found, corners = cv2.findChessboardCorners(img, pattern_size)
    if found:
        term = ( cv2.TERM_CRITERIA_EPS + cv2.TERM_CRITERIA_COUNT, 30, 0.1)
        cv2.cornerSubPix(img, corners, (4, 4), (-1, -1), term)
    if debug_dir:
        vis = cv2.cvtColor(img, cv2.COLOR_GRAY2BGR)
        cv2.drawChessboardCorners(vis, pattern_size, corners, found)
        path, name, ext = splitfn(fn)
        cv2.imwrite('%s/%s_chess.bmp' % (debug_dir, name), vis)
    if not found:
        print 'chessboard not found'
        continue
    img_points.append(corners.reshape(-1, 2))
    obj_points.append(pattern_points)

    print 'ok'

rms, camera_matrix, dist_coefs, rvecs, tvecs = cv2.calibrateCamera(obj_points, img_points, (w, h))
#f = open("C:\Users\Administrator\Desktop\calibration_camera.xml","w")
print "RMS:", rms
print "camera matrix:\n", camera_matrix
print "distortion coefficients: ", dist_coefs.ravel()

print "Printing camera characteristics"
fovx,fovy,f,principalPoint,aspect = cv2.calibrationMatrixValues(camera_matrix, img.shape[:2], 2, 2)

print fovx,fovy ,f

RMS = 0.362852017935
camera_matrix=[[ 783.48964894 ,   0.        ,  296.49364884],
 [   0.         , 784.46632862,  191.44401818],
 [   0.         ,   0.        ,    1.        ]]
dist_coefs= [ -4.18590121e-01   ,2.65146495e+00 , -7.09289106e-03  ,-3.77973579e-03,
  -7.21579887e+00]

#Dumping data into a YAML file
data = {"camera_matrix":camera_matrix,"distortion_coefficients":dist_coefs}
import yaml
fname = "Left.yaml"
with open(fname,"w") as f:
    yaml.dump(data,f)


# np.savez('abcd', camera_matrix, dist_coefs)





#############################################################################################################