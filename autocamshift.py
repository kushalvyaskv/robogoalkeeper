#!/usr/bin/env python


import numpy as np
import ctypes
import cv2
import threading
from threading import Thread
from multiprocessing import Process,Value,Array,Lock
import sys
import multiprocessing
# from compute import Compute as Compute
# from compute import *
import time
import yaml
import serial
import sys
from arduinoCommunicator import arduinoCommunicator

class App(multiprocessing.Process):
    def __init__(self, video_src,threadID):
        # threading.Thread.__init__(self);
        multiprocessing.Process.__init__(self)

        self.lower = np.array([60,100,150],dtype=np.uint8);
        self.upper = np.array([150,160,250],dtype=np.uint8);
        

        self.threadID = threadID
        self.process = multiprocessing.current_process()
        # declaring compute object
        # self.computeObj = Compute(self.threadID)

        self.cam = cv2.VideoCapture(video_src)
        for i in range(15):
            ret, self.frame = self.cam.read()
        
        # cv2.namedWindow('camshift')
        
        self.selection = 1
        self.drag_start = None
        self.tracking_state = 0
        self.show_backproj = False
       

    def loadData(self, filename):
        self.filename = filename
        print self.threadID
        try:
            print self.filename
            self.f = open(self.filename)
            print self.f
            data = yaml.safe_load(self.f)
            self.f.close()
            self.camera_matrix = np.asarray(data['camera_matrix'])
            self.distortion_coefficients = np.asarray(data['distortion_coefficients'])
            # self.rvecs =np.asarray(data['rvecs'])
            # self.tvecs =np.asarray(data['tvecs'])
            print 'Camera data loaded'
            print self.camera_matrix
            print self.distortion_coefficients
            # print self.rvecs , self.tvecs
            print 'done initializing'
        except IOError as e1:
            print 'file not found',e1
        except Exception as e:
            print "Error in reading file",e;


    def thresh(self):
        # kernel = np.ones((6,6),np.uint8)
        # self.lower = np.array([0,0,0],dtype=np.uint8);
        # self.upper = np.array([135,255,255],dtype=np.uint8);
        # self.lower = np.array([60,50,50],dtype=np.uint8);
        # self.upper = np.array([135,255,255],dtype=np.uint8);

        hsv = cv2.cvtColor(self.frame,cv2.COLOR_BGR2HSV)
        hsv = cv2.inRange(hsv,self.lower,self.upper)
        # cv2.imshow('hsv',hsv)
        # cv2.waitKey()
        # hsv = cv2.morphologyEx(hsv, cv2.MORPH_OPEN, kernel)
        # hsv = cv2.erode(hsv,kernel,iterations=1)
        # hsv = cv2.dilate(hsv , kernel,iterations=1)
        x=[];y=[]   
        contours, hierarchy = cv2.findContours(hsv,cv2.RETR_LIST,cv2.CHAIN_APPROX_SIMPLE)
        for cnt in contours:
            if len(cnt):
                M = cv2.moments(cnt)
                try:
                    x.append(int(M['m10']/M['m00']))
                    y.append(int(M['m01']/M['m00']))

                except:
                    pass
                # cv2.drawContours(hsv, [cnt], -1, (0,255,0), 3)
        # print x,y
        try:
            xx = int(np.mean(x));yy = int(np.mean(y))
        except:
            print ' Estimated centroid at 50,50 '
            xx,yy=50,50
        # cv2.imshow("HSV1",hsv)
        # cv2.waitKey()
        return xx-20,yy-20,xx+20,yy+20

   

    def run(self,X,Y,lock):
        # print X,Y,lock
        __,self.frame = self.cam.read()
        global system_exit,computeObj  #,left_camera_thread
        """
        h,w = self.frame.shape[:2]
        newcameramtx , roi = cv2.getOptimalNewCameraMatrix(self.camera_matrix,self.distortion_coefficients,(w,h),1,(w,h))
        # undistorting the imge
        self.frame = cv2.undistort(self.frame,self.camera_matrix,self.distortion_coefficients,None,newcameramtx)
        x,y,w,h = roi
        self.frame = self.frame[y:y+h, x:x+w]
        """
        x0, y0, x1, y1 = self.thresh()
        print x0,y0,x1,y1
        # time.sleep(3)

        while True:
            ret, self.frame = self.cam.read()
            """
            h,w = self.frame.shape[:2]
            newcameramtx , roi = cv2.getOptimalNewCameraMatrix(self.camera_matrix,self.distortion_coefficients,(w,h),1,(w,h))
            # undistorting the imge
            self.frame = cv2.undistort(self.frame,self.camera_matrix,self.distortion_coefficients,None,newcameramtx)
            x,y,w,h = roi
            self.frame = self.frame[y:y+h, x:x+w]
            """
            # image copy for display
            vis = self.frame.copy()
            # thresholding
            hsv = cv2.cvtColor(self.frame, cv2.COLOR_BGR2HSV)
            mask = cv2.inRange(hsv,self.lower,self.upper)
            # mask = cv2.inRange(hsv, np.array((0., 60., 32.)), np.array((180., 255., 255.)))
            if True:
                if self.selection:
                    # sets the sizr of the tracking window
                    self.track_window = (x0, y0, x1-x0, y1-y0)
                    hsv_roi = hsv[y0:y1, x0:x1]
                    mask_roi = mask[y0:y1, x0:x1]
                    # histogram calculations
                    hist = cv2.calcHist( [hsv_roi], [0], mask_roi, [16], [0, 180] )
                    cv2.normalize(hist, hist, 0, 255, cv2.NORM_MINMAX);
                    self.hist = hist.reshape(-1)
                    # enabling the tracking module
                    self.tracking_state=1

                    vis_roi = vis[y0:y1, x0:x1]
                    # inverting the roi for b_T
                    cv2.bitwise_not(vis_roi, vis_roi)
                    vis[mask == 0] = 0

                    # cv2.imshow('self.selection',vis)
                    # cv2.waitKey()

                if self.tracking_state == 1 :
                    self.selection = None
                    prob = cv2.calcBackProject([hsv], [0], self.hist, [0, 180], 1)
                    prob &= mask
                    term_crit = ( cv2.TERM_CRITERIA_EPS | cv2.TERM_CRITERIA_COUNT, 10, 1 )
                    with lock:
                        try:
                            # camshift algorithm implementaion
                            track_box, self.track_window = cv2.CamShift(prob, self.track_window, term_crit)
                            x,y = [int(X1) for X1 in track_box[0]]
                            # print "return value of position centroid is = ",[x,y]
                            print 'x and y returned are :',x,y
                            try:
                                if self.threadID == 'LEFT':
                                    X.value = int(x)
                                    Y.value = int(y)
                                elif self.threadID == 'RIGHT':
                                    X.value = int(x)
                                    Y.value = int(y)
                                else:
                                    pass
                            except Exception, e:
                                # print 'X and Y are: ',
                                print e
                            
                            # print self.threadID,X.value , Y.value
                            computeObj.setValues()
                            computeObj._3D()
                           
                            
                            print self.threadID
                            # print X.value,Y.value,self.threadID
                        except:
                            # print "Unable to determine object"
                            # again start searching for the object
                            x0, y0, x1, y1 = self.thresh()
                            self.selection=1
                            self.tracking_state=0
                            print 'in except block'
                            # pass
                            # self.computeObj.setValues([9999,9999])
                            # print "in except block"
                            # call the kalman filter
                        # if self.show_backproj:
                        #     vis[:] = prob[...,np.newaxis]


                    

                    try: cv2.ellipse(vis, track_box, (0, 0, 255), 2)
                    except: print track_box



            # except:
            #    pass
            

            cv2.imshow(self.threadID, vis)

            ch = 0xFF & cv2.waitKey(5)
            if ch == 27:

                system_exit = True
                print "Exit values = ",system_exit
                cv2.destroyAllWindows()
                break
           
        cv2.destroyAllWindows()


class Compute(App):
    def __init__(self):
        print 'constructor initializing'
        self.left_camera_coordinates=[]
        self.right_camera_coordinates=[]
        self.focal_length = 6.79243932e+02 
        self.base_distance = 80.0 #in cm 
        # focal length is calculated as per camera matrix
        self.disparity = 1
        self.depth =0
        self.x3D,self.y3D,self.z3D=0,0,0;
    def setValues(self):
        self.left_camera_coordinates = [xL.value,yL.value]
        self.right_camera_coordinates = [xR.value,yR.value]
        self.disparity  = self.left_camera_coordinates[0]-self.right_camera_coordinates[0]
    def _3D(self):
        try:
            self.depth = self.focal_length * self.base_distance/ self.disparity
            self.x3D = self.left_camera_coordinates[0] * self.depth /self.focal_length;
            self.y3D = self.left_camera_coordinates[1] * self.depth /self.focal_length;
            self.z3D = self.depth;
            
        except Exception, e:
            print e
        finally:
            print "WRT Left camera frame : 3D coordintate are : ",'(',self.x3D,',' ,self.y3D,',',self.z3D,')';

    def write_data_to_arduino(self):
        
        pass
   

if __name__ == '__main__':
    
    system_exit = False

    # xL,yL,xR,yR 
    xL=Value('d',0)
    yL=Value('d',0)
    xR=Value('d',0)
    yR=Value('d',0);
    print xL.value,yL.value,xR.value,yR.value
    lock  =Lock();
    computeObj = Compute()

    s = App(2,'LEFT')
    l = App(1,'RIGHT')
    l.loadData('Right.yaml')
    s.loadData('Left.yaml')

    # p.map(App.run,(l,s))
    
    right_camera_thread = Process(target=l.run,args=(xR,yR,lock,))
    left_camera_thread = Process(target=s.run,args=(xL,yL,lock,))
    right_camera_thread.start()
    left_camera_thread.start()
    
   
    # while True:
    #     print system_exit
    #     if system_exit:
    #         left_camera_thread.terminate()
    #         system_camera_thread.terminate()



    exit(0)