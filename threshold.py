import cv2
import numpy as np 

img = cv2.imread('image.png')

lower = np.array([60,100,150],dtype=np.uint8);
upper = np.array([150,160,250],dtype=np.uint8);


i = cv2.cvtColor(img,cv2.COLOR_BGR2HSV);
i = cv2.inRange(i,lower,upper)
# cv2.imshow('img',img)
cv2.imshow('i',i)
cv2.waitKey()
cv2.destroyAllWindows()
