import cv2
import sys
import numpy as np 
import os
import yaml
from glob import glob

def loadData():
    #extracting values for left camera
    print 'Loading Left camera data ...'
    f = open("Right.yaml")
    dataLeft = yaml.safe_load(f)
    f.close()
    mtx = np.asarray(dataLeft["camera_matrix"])
    dist = np.asarray(dataLeft["distortion_coefficients"])
    # rvecs_left=np.asarray(dataLeft["rvecs_left"])
    # tvecs_left=np.asarray(dataLeft["tvecs_left"])
    print 'Done'
    return mtx,dist


img = cv2.imread('right//image1.png')
h,  w = img.shape[:2]
mtx,dist = loadData()
mtx=np.asarray(mtx)
dist = np.asarray(dist)
newcameramtx, roi=cv2.getOptimalNewCameraMatrix(mtx,dist,(w,h),1,(w,h))
# undistort
dst = cv2.undistort(img, mtx, dist, None, newcameramtx)
print mtx , newcameramtx
# crop the image
x,y,w,h = roi
dst = dst[y:y+h, x:x+w]
cv2.imwrite('calibresult.png',dst)

